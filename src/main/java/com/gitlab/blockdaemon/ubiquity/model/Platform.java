package com.gitlab.blockdaemon.ubiquity.model;

/**
 * Non-exhaustive list of supported platforms
 *
 */
public enum Platform {

	ALGORAND("algorand"), BITCOIN("bitcoin"), CELO("celo"), DIEM("diem"), ETHEREUM("ethereum"), OASIS("oasis"),
	POLKADOT("polkadot"), RIPPLE("ripple"), OTHER();

	private String platform;

	Platform(String platform) {
		this.platform = platform;
	}

	Platform() {
	}

	public String getPlatform() {
		return platform;
	}

	public static Platform name(String platform) {
		Platform otherPlatform = Platform.OTHER;
		otherPlatform.platform = platform;
		return otherPlatform;
	}
}
