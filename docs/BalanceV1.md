

# BalanceV1

Currency balances with asset paths as keys

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**currency** | [**Currency**](Currency.md) |  |  [optional]
**confirmedBalance** | **String** |  |  [optional]
**pendingBalance** | **String** |  |  [optional]
**confirmedNonce** | **Integer** |  |  [optional]
**confirmedBlock** | **Integer** |  |  [optional]



