

# ReportFieldMeta

Additional metadata bespoke to specific protocols

## oneOf schemas
* [AlgorandMeta](AlgorandMeta.md)

NOTE: this class is nullable.

## Example
```java
// Import classes:
import com.gitlab.blockdaemon.ubiquity.model.ReportFieldMeta;
import com.gitlab.blockdaemon.ubiquity.model.AlgorandMeta;

public class Example {
    public static void main(String[] args) {
        ReportFieldMeta exampleReportFieldMeta = new ReportFieldMeta();

        // create a new AlgorandMeta
        AlgorandMeta exampleAlgorandMeta = new AlgorandMeta();
        // set ReportFieldMeta to AlgorandMeta
        exampleReportFieldMeta.setActualInstance(exampleAlgorandMeta);
        // to get back the AlgorandMeta set earlier
        AlgorandMeta testAlgorandMeta = (AlgorandMeta) exampleReportFieldMeta.getActualInstance();
    }
}
```


