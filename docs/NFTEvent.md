

# NFTEvent


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**contractAddress** | **String** |  |  [optional]
**tokenId** | **Long** |  |  [optional]
**eventType** | **String** |  |  [optional]
**timestamp** | **Long** |  |  [optional]
**fromAccount** | **String** |  |  [optional]
**toAccount** | **String** |  |  [optional]
**transaction** | **Object** |  |  [optional]



