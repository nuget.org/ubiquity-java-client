

# MultiTransferOperation


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **String** |  | 
**event** | **String** |  |  [optional]
**detail** | [**MultiTransfer**](MultiTransfer.md) |  | 



